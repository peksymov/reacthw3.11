import React, { useState, useEffect } from 'react'
import CardList from "../../components/CardList";
import phonesListJSON from "../../services";
import {favoritesPhonesLocalSt} from '../../services/index-json'


export default function Favorites (props) {

    const someArr = []
    async function her() {
        const {data} = await phonesListJSON();
        data.map(item => {
            someArr.push(item)
        })
    }
    her()

    const [favorites, setFavorites] = useState([])

    useEffect(() => {
        async function jsonPhonesArray() {
            const {data} = await phonesListJSON();
            const arrayLocalGet = favoritesPhonesLocalSt();
            const newArray = []
            data.forEach(item => {
                if (arrayLocalGet === null) {
                    setFavorites([])
                }
                else if (arrayLocalGet.includes(item.vendor_code)) {
                    newArray.push(item)
                }
            })
            setFavorites(newArray);
        }
        jsonPhonesArray();
    }, [])

    const deleteFromFavoritesPage = () => {
        const arrayLocalGet = favoritesPhonesLocalSt();
        const newArray = []
        someArr.forEach(item => {
            if (arrayLocalGet.includes(item.vendor_code)) {
                newArray.push(item)
            }
        })
        setFavorites(newArray)
    }

    return (
        <CardList
            phones={favorites}
            favorites2={favorites}
            deleteFromFavoritesPage={deleteFromFavoritesPage}
            setPhonesBasket={props.setPhonesBasket}
            setPhonesFavorite={props.setPhonesFavorite}
        />
    )
}