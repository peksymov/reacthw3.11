import React, {useEffect, useState} from 'react'
import CardList from "../../components/CardList";
import axios from "axios";
import phonesListJSON from "../../services";

export default function Home (props) {

    const [phones, setPhones] = useState([])

    useEffect(() => {
        async function jsonPhonesArray() {
            const {data} = await phonesListJSON();
            setPhones(data);
        }
        jsonPhonesArray();
    }, [])


    return (
        <div>
            <CardList
                phones={phones}
                setPhonesBasket={props.setPhonesBasket}
                setPhonesFavorite={props.setPhonesFavorite}
            />
        </div>

    )
}