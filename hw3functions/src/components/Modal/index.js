import React, {useState} from 'react'
import './Modal.scss'
import Button from "../Button";


function Modal(props) {

    return (
        <div className='modal' onClick={props.openModals}>
            <div className='modal-body' onClick={e => e.stopPropagation()}>
                <div className='modalHeader'>
                    <p> Телефон
                        <span className='modal-card-span'> {props.namePhone}</span> добавлен в корзину
                        <span className='closeCross' onClick={props.openModals}>&times;</span>
                    </p>
                    <img className='modal-image' src={props.imageUrl} alt=""/>
                    <div>
                        <Button btnText='Add to modal' className='buttonsDesign modal-btn'/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Modal