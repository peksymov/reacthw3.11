import React, {useEffect, useState} from 'react'
import './Card.scss'
import Button from "../Button";
import Modal from "../Modal";
import {favoritesPhonesLocalSt, basketPhonesLocalSt} from '../../services/index-json'
// import basketPhonesLocalSt from "../../services/index-json";

function Card (props) {

    const [showStar, setShowStar] = useState(false)
    useEffect(() => {
        function showMeRightStar() {
            // const getFavoriteLocal = JSON.parse(localStorage.getItem('favoritePhones3'));
            const getFavoriteLocal = favoritesPhonesLocalSt();
            if (getFavoriteLocal === null || getFavoriteLocal) {
                setShowStar(false)
            }
            if (getFavoriteLocal !== null) {
                getFavoriteLocal.forEach(item => {
                    if (item === props.phone.vendor_code) {
                        setShowStar(true)
                    }
                })
            }
        }
        showMeRightStar()
    })

    const [btnRed, setBtnRed] = useState(false)

    useEffect(() => {
        function showMeRightBtn() {
            // const getBasketLocal = JSON.parse(localStorage.getItem('basketPhones3'));
            const getBasketLocal = basketPhonesLocalSt();
            if (getBasketLocal === null || getBasketLocal.length === 0) {
                setBtnRed(false)
            }
            if (getBasketLocal !== null) {
                getBasketLocal.forEach(item => {
                        if (item === props.phone.vendor_code) {
                            setBtnRed(true)
                        }
                })
            }
        }
        showMeRightBtn()
    })

    const [openModal, setOpenModal] = useState(false)
    const closeModals = () => {
        setOpenModal(true)
    }
    const openModals = () => {
        setOpenModal(false)
    }
    const dataPhoneFavorite = () => {

        // const favoritesGetLocal = JSON.parse(localStorage.getItem('favoritePhones3'));
        const favoritesGetLocal = favoritesPhonesLocalSt()

        if (favoritesGetLocal === null) {
            const favoritesGetLocal = [];
            favoritesGetLocal.push(props.phone.vendor_code)
            localStorage.setItem('favoritePhones3',JSON.stringify(favoritesGetLocal));
            props.setPhonesFavorite(prev=> prev + 1)
        }
        if (favoritesGetLocal !== null) {
            const vendor_code = props.phone.vendor_code
            if (favoritesGetLocal.includes(vendor_code)) {
                const newArray = favoritesGetLocal.filter(card => card !== vendor_code)
                localStorage.setItem('favoritePhones3',JSON.stringify(newArray));
                setShowStar(false)
                props.setPhonesFavorite(prev=> prev - 1)
                if (props.favorites2) {
                    props.deleteFromFavoritesPage()
                }
            }
            else {
                favoritesGetLocal.push(vendor_code)
                localStorage.setItem('favoritePhones3',JSON.stringify(favoritesGetLocal));
                setShowStar(true)
                props.setPhonesFavorite(prev=> prev + 1)
            }
        }

    }

    const dataPhoneBasket = () => {
        // const basketGetLocal = JSON.parse(localStorage.getItem('basketPhones3'));
        const basketGetLocal = basketPhonesLocalSt();

        if (basketGetLocal === null) {
            const basketGetLocal = [];
            basketGetLocal.push(props.phone.vendor_code)
            localStorage.setItem('basketPhones3', JSON.stringify(basketGetLocal));
                setBtnRed(true)
                setOpenModal(true)
                props.setPhonesBasket(prev => prev + 1)
        }

        if (basketGetLocal !== null) {
            const vendor_code = props.phone.vendor_code
            if (basketGetLocal.includes(vendor_code)) {
                const newArray = basketGetLocal.filter(card => card !== vendor_code)
                localStorage.setItem('basketPhones3',JSON.stringify(newArray));
                setBtnRed(false)
                setOpenModal(false)
                props.setPhonesBasket(prev => prev - 1)
                if (props.baskets2) {
                    props.deleteCardFromBasket()
                }
            }
            else {
                basketGetLocal.push(vendor_code)
                localStorage.setItem('basketPhones3',JSON.stringify(basketGetLocal));
                setBtnRed(true)
                setOpenModal(true)
                props.setPhonesBasket(prev => prev + 1)
            }
        }
    }


    const {vendor_code,imageUrl,namePhone,price,color} = props.phone
    return (
        <div className='card' key={vendor_code}>
            <div className='img-wrapper'>
                <img src={imageUrl} className="card-img-top" alt="Фото Телефона"/>
            </div>
            <div className='card-body'>
                <h5 className="card-title">Model: {namePhone}</h5>
                <p className="card-text">Price: {price}</p>
                <p className="card-text-color">Color: {color}</p>
                {!showStar &&
                <i className='fas fa-star redStar' onClick={dataPhoneFavorite}>Добавить в избранное</i>
                }
                {showStar &&
                <i className='fas fa-star blueStar' onClick={dataPhoneFavorite}>Убрать из избранного</i>
                }
                <div className='add-remove-container'>
                    {!btnRed &&
                    <Button
                        className='buttonsDesign card-add'
                        btnText='Add to Basket'
                        onClick={dataPhoneBasket}
                    />
                    }
                    {btnRed &&
                    <Button
                        className='buttonsDesign modal-remove-basket-all'
                        btnText='Remove from Basket'
                        onClick={dataPhoneBasket}
                    />
                    }
                </div>
                {openModal &&
                <Modal
                    imageUrl={imageUrl}
                    namePhone={namePhone}
                    openModalBody={closeModals}
                    openModals={openModals}
                />
                }
            </div>
        </div>
    )
}

export default Card