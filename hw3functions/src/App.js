import React, {useState, useEffect} from 'react'
import './App.css';
import {Link} from "react-router-dom";
import Main from './components/Main'
import {favoritesPhonesLocalSt, basketPhonesLocalSt} from './services/index-json'

function App() {

    const [phonesFavorite, setPhonesFavorite] = useState(0)
    const [phonesBasket, setPhonesBasket] = useState(0)


    useEffect(() => {
        function showMeGoodsInBasket() {
            const getBasketLocal = basketPhonesLocalSt()
            if (getBasketLocal === null || getBasketLocal.length === 0) {
                setPhonesBasket(0)
            }
            else {
                const BasketLength = getBasketLocal.length
                setPhonesBasket(BasketLength)
            }
        }
        showMeGoodsInBasket()
    },[])

    useEffect(() => {
        function showMeGoodsInFavorites() {
            const getFavoriteLocal = favoritesPhonesLocalSt();
            if (getFavoriteLocal === null || getFavoriteLocal.length === 0) {
                setPhonesFavorite(0)
            }
            else {
                const FavoriteLength = getFavoriteLocal.length
                setPhonesFavorite(FavoriteLength)
            }
        }
        showMeGoodsInFavorites()
    },[])



    return (
        <div className="App">
            <div>
                <Link to={`/`} >AllPhones</Link>
                <Link to={`/favorites/`} >Favorites <span className='link-decoration'>{phonesFavorite}</span></Link>
                <Link to={`/baskets/` } >BasketList <span className='link-decoration'>{phonesBasket}</span></Link>
            </div>
            <Main
                setPhonesBasket={setPhonesBasket}
                setPhonesFavorite={setPhonesFavorite}
            />
        </div>
    );
}

export default App;
